<?php

namespace App\Form;

use App\Entity\Ad;
use App\Entity\Media;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Titre de l'annonce"
            ])
            ->add('description', CKEditorType::class, [
                'label' => "Description de l'annonce"
            ])
            ->add('price', IntegerType::class, [
                'label' => 'Prix'
            ])
            ->add('rooms', IntegerType::class, [
                'label' => 'Nombre de pièces'
            ])
            ->add('bedrooms', IntegerType::class, [
                'label' => 'Nombre de chambres'
            ])
            ->add('bathrooms', IntegerType::class, [
                'label' => 'Nombre de salle de bains'
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse',
                'required' => false
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'Code postal',
                'required' => false,
                'attr' => [
                    'maxLength' => 5
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
                'required' => false
            ])
            ->add('type', ChoiceType::class, [
                'expanded' => false,
                'multiple' => false,
                'choices' => [
                    'Location' => Ad::CODE_RENT,
                    'Vente' => Ad::CODE_SALE
                ]
            ])
            ->add('codeType', ChoiceType::class, [
                'expanded' => false,
                'multiple' => false,
                'choices' => [
                    'Appartement' => Ad::CODE_TYPE_FLAT,
                    'Maison' => Ad::CODE_TYPE_HOUSE
                ]
            ])
            ->add('cover', MediaType::class, [
                'label' => 'Image de couverture',
                'required' => false
            ])
            ->add('medias', CollectionType::class, [
                'label' => "Images d'illustration",
                'entry_type' => MediaType::class,
                'entry_options' => [
                    'label' => false
                ],
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => static function(Media $media) {
                    return $media->getName() === null && $media->getFile() === null;
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
