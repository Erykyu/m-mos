<?php

namespace App\EventSubscriber;

use App\Entity\Media;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class RemoveMediaSubscriber implements EventSubscriber
{

    /**
     * @var UploaderHelper
     */
    private $helper;
    /**
     * @var CacheManager
     */
    private $cacheManager;

    public function __construct(UploaderHelper $helper, CacheManager $cacheManager)
    {
        $this->helper = $helper;
        $this->cacheManager = $cacheManager;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::preUpdate,
            Events::preRemove
        ];
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!($entity instanceof Media)) {
            return;
        }

        if ($entity->getFile() instanceof UploadedFile) {
            $this->cacheManager->remove($this->helper->asset($entity, 'file'));
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!($entity instanceof Media)) {
            return;
        }

        $this->cacheManager->remove($this->helper->asset($entity, 'file'));
    }
}
