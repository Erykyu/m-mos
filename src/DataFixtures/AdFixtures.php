<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AdFixtures extends Fixture
{
    public function load(ObjectManager $manager): void 
    {
        $faker = Factory::create('fr_FR');
        
        for ($i = 0; $i < 30; $i++) {
            $ad = new Ad();
            $ad
                ->setName($faker->sentence)
                ->setDescription('<p>' . implode('</p><p>', $faker->paragraphs(5)) . '</p>')
                ->setPrice($faker->randomNumber())
                ->setRooms($faker->numberBetween(2, 5))
                ->setBedrooms($faker->numberBetween(0, 2))
                ->setBathrooms($faker->numberBetween(0, 1))
                ->setAddress($faker->boolean ? $faker->streetAddress : null)
                ->setZipCode($faker->boolean ? $faker->postcode : null)
                ->setCity($faker->city)
                ->setType($faker->randomElement([
                    Ad::CODE_RENT,
                    Ad::CODE_SALE
                ]))
                ->setCodeType($faker->randomElement([
                    Ad::CODE_TYPE_FLAT,
                    Ad::CODE_TYPE_HOUSE
                ]));

            $manager->persist($ad);
        }

        $manager->flush();
    }
}
