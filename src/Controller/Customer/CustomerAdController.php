<?php

namespace App\Controller\Customer;

use App\Entity\Ad;
use App\Form\AdType;
use App\Repository\AdRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CustomerAdController extends AbstractController
{
    /**
     * @Route("/account/ad", name="customer_ad_index")
     */
    public function index(AdRepository $adRepository): Response
    {
        return $this->render('ad/index.html.twig', [
            'ads' => $adRepository->findAll()
        ]);
    }

    /**
     * @Route("/account/ad/create", name="customer_ad_create")
     */
    public function create(Request $request, ObjectManager $manager): Response
    {
        $ad = new Ad();

        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($ad);
            $manager->flush();

            return $this->redirectToRoute('customer_ad_index');
        }

        return $this->render('ad/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/account/ad/{ad}", name="customer_ad_edit", methods={"GET", "POST"})
     */
    public function edit(Ad $ad, Request $request, ObjectManager $manager): Response
    {
        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();

            return $this->redirectToRoute('customer_ad_index');
        }

        // TODO: flash

        return $this->render('ad/create.html.twig', [
            'ad' => $ad,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/account/ad/{ad}", name="customer_ad_delete", methods={"DELETE"}, options={"expose"=true})
     */
    public function delete(Ad $ad, Request $request, ObjectManager $manager): RedirectResponse
    {
        if (!$this->isCsrfTokenValid("delete{$ad->getId()}", $request->request->get('_token'))) {
            return $this->redirectToRoute('customer_ad_index');
        }

        // TODO: flash

        $manager->remove($ad);
        $manager->flush();

        return $this->redirectToRoute('customer_ad_index');
    }
}
