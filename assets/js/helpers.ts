const images = new Set<string>();

export function addElementToCollection(collection: HTMLElement): void {
  const prototype = collection.dataset.prototype;
  const index = collection.dataset.index;

  const newElement = fromTemplate(prototype.replace(/__name__/g, index)) as HTMLElement;

  const input = newElement.querySelector<HTMLInputElement>('input[type="file"]');
  input.addEventListener('change', () => {
    inputFileChangeValue(input);
  });

  collection.dataset.index = (Number(collection.dataset.index) + 1).toString();
  collection.appendChild(newElement);

  addDeleteButtonToCollection(newElement);
}

export function inputFileChangeValue(input: HTMLInputElement) {
  const files = input.files;

  if (files && files.length > 0) {
    // Pour transformer l'image en blob
    const url = URL.createObjectURL(files[0]);
    images.add(url);

    const current_image = document.querySelector<HTMLImageElement>(`img[data-refer-to="${input.id}"]`);

    if (current_image) {
      const old_image = current_image.src;

      if (images.has(old_image)) {
        URL.revokeObjectURL(old_image);
        images.delete(old_image);
      }

      current_image.src = url;
    }

    document.querySelector<HTMLLabelElement>(`label[for="${input.id}"]`).innerText = files[0].name;
  }
}

export function addDeleteButtonToCollection(element: HTMLElement): void {
  const deleteButton = document.createElement('button');
  deleteButton.classList.add('btn', 'btn-danger');
  deleteButton.type = 'button';
  deleteButton.innerText = 'Supprimer';

  element.getElementsByClassName('js-button-delete')[0].appendChild(deleteButton);
  element.dataset.hasDeleteButton = 'true';

  deleteButton.addEventListener('click', () => {
    element.remove();
  });
}

function fromTemplate(template: string) {
  const t = document.createElement('template');

  t.innerHTML = template;
  return t.content.firstElementChild;
}
