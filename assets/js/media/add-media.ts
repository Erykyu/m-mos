import { addDeleteButtonToCollection, addElementToCollection, inputFileChangeValue } from '../helpers';

document.addEventListener('DOMContentLoaded', () => {
  const mediaCollection = document.getElementById('ad_medias');

  mediaCollection.dataset.index = mediaCollection.getElementsByTagName('input').length.toString();

  document.getElementById('js-add-media').addEventListener('click', () => {
    addElementToCollection(mediaCollection);
  });

  for (const input of document.querySelectorAll<HTMLInputElement>('input[type="file"]')) {
    input.addEventListener('change', () => {
      inputFileChangeValue(input);
    });

    if (input.id === 'ad_cover_file') {
      continue;
    }

    let fieldset = input.parentElement;
    while (fieldset.tagName !== 'FIELDSET') {
      fieldset = fieldset.parentElement;
    }

    addDeleteButtonToCollection(fieldset);
  }
});
