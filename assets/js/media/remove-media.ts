import { Routing } from '../routing';

for (const deleteBtn of document.querySelectorAll<HTMLButtonElement>('button[data-toggle="modal"]')) {
  deleteBtn.addEventListener('click', () => {
    const ad = deleteBtn.dataset.ad;
    const token = deleteBtn.dataset.token;

    const form = document.querySelector<HTMLFormElement>('#js-delete-media');

    form.action = Routing.generate('ad_delete', {ad});
    form.querySelector<HTMLInputElement>('input[type="hidden"][name="_token"]').value = token;
  });
}
